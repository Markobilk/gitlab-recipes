## GitLab recipes: unofficial guides for using GitLab with different software

For the official installation options for GitLab and other unofficial ones please see the [GitLab installation page](https://about.gitlab.com/install/).

For configuring the Omnibus packages with a non-packaged webservers, database, redis please see the [Omnibus docs](http://doc.gitlab.com/omnibus/).

In this repository you will find unofficial guides for using GitLab with different software (operating systems, web servers, etc.)
provided by the community, for systems other than the officially supported (Debian/Ubuntu).

Bear in mind that this repository is co-maintained by volunteers/contributors like you and currently isn't very active.

## Contributing

See [contributing.md](CONTRIBUTING.md) for contribution guidelines.

## Notes on the reliability of guides/scripts

* We will try to test everything before accepting PRs, in a clean, newly installed platform.
* You should read a script and understand what it does prior to running it.
* If something goes wrong during installation and you think the guide/script needs fixing, file a bug report or a submit a Pull Request.
